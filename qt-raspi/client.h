#ifndef CLIENT_H
#define CLIENT_H

#include <QMainWindow>
#include <QtSql>
#include <QDebug>
#include <QSqlTableModel>
#include <QTableView>
#include <QTextBrowser>
#include <QTextEdit>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QComboBox>
#include <QDateTimeAxis>
#include <QValueAxis>
#include <QDateTime>
#include <QBarCategoryAxis>


QT_BEGIN_NAMESPACE
namespace Ui { class Client; }
QT_END_NAMESPACE

class Client : public QMainWindow
{
    Q_OBJECT

public:
    Client(QWidget *parent = nullptr);
    ~Client();

private slots:
    void on_graphButton_clicked();
    void on_refreshButton_clicked();

    void dallasTemperature();
    void dhtTemperature();
    void dhtHumidity();

private:
    Ui::Client *ui;
};
#endif // CLIENT_H
