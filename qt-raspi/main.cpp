/*----------------------------------------------------*/

//You need to install "qt5-charts" package,
//if you can't compile or run the program

/*----------------------------------------------------*/

#include "client.h"
#include <QApplication>

void databaseSettings();

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    databaseSettings();
    Client w;
    w.show();
    return a.exec();
}


//Database Settings
void databaseSettings()
{
    //Database connect settings
    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL"); // PostgreSQL driver
    db.setHostName("x.x.x.x"); // set the name of your host
    db.setDatabaseName("raspi"); // set the name of your database
    db.setUserName("postgres"); // set the name of user, who is owner of the database
    db.setPassword("password"); // set the password of the user
}
