# Qt Projekti
## Suomeksi
### Projektin Tavoitteet
Projektin tavoite oli luoda qt-sovellus, jolla pystytään lukemaan databasea ja visualisoimaan sen dataa. Datana on DHT anturin lämpätila & kosteus ja dallas anturin lämpötila.

### Taustaa 
Proketi on tehty ammattikorkeakoulun 2- vuoden aikana

### Parannettavaa
- Projektin ulkonäkö voisi olla mukavampi tai enemmän moderni.
- UI:ta ei ole käyttäjä testattu --> Ei ole vältämättä hyvä käyttäjä kokemus...

### Huomioitavaa
Ei toimi suoraan, koska databasen IP ja salasana ovat placeholdereita



## In English
### Project End Goals
Projects end goal was to create qt program that can read a database and visualisze the data that is stored in the database. The data includes DHT sensors temperature & humidity and dallas sensors temperature.

### Context  
This project was made in the second year of study at the university of applied sciences.

### Improvements
- Programs visual side could be better or more modern.
- The UI is not user tested --> It might not be a good user experince to use. 

### Note 
The program does not work straight out of the box because the database IP and password settings uses placeholders.
